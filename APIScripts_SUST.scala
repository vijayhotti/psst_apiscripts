package io.gatling.highcharts

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class APIScripts_SUST extends Simulation {

  val hp_feed = tsv("hp_feeder.tsv").random
  val product_feeder = csv("product_feeder.csv").random
  val seo_feeder = csv("seo_feeder.csv").random
  val inventory_feeder = csv("inventory_feeder.csv").random
  val search_feeder = csv("search_feeder.csv").random

  val httpProtocol = http
    .baseURL("http://detectportal.firefox.com")
    .inferHtmlResources(BlackList(""".*\.js""", """.*\.css""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.(t|o)tf""", """.*\.png"""), WhiteList())
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0")

  val headers_0 = Map("Pragma" -> "no-cache")

  val headers_3 = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "DNT" -> "1",
    "Upgrade-Insecure-Requests" -> "1")

  val uri1 = "http://35.202.244.154/api/"

  //val uri2 = "http://35.202.235.124/api/"

  object Product {
    val Carousel = feed(product_feeder).exec(http("Prdt_Carousel")
      .get(uri1 + "productinfo?productIds=${prod}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val APIByProduct = feed(product_feeder).exec(http("Prdt_PDP")
      .get(uri1 + "productinfo/${prod}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val FacetCategory = feed(product_feeder).exec(http("Prdt_FacetCategory")
      .get(uri1 + "category/${cat}/products?facets=true").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
  }

  object Category {
    val MultiCategory = feed(hp_feed).exec(http("Categ_MultiCategory")
      .get(uri1 + "category?categoryIds=${category}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
  }

  object Seo {
    val ProductName = feed(seo_feeder).exec(http("Seo_ProductName")
      .get(uri1 + "variant?prodSeoName=${Product}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val CategoryName = feed(seo_feeder).exec(http("Seo_CategoryName")
      .get(uri1 + "variant?catSeoName=${Category}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
  }

  object Inventory {
    val OnlineAndStore = feed(inventory_feeder).exec(http("Invt_OnlineAndStore")
      .get(uri1 + "inventory?productId=${ProductID}&storeId=${StoreID}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val Online = feed(inventory_feeder).exec(http("Invt_Online")
      .get(uri1 + "inventory/${ProductID}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val Store = feed(inventory_feeder).exec(http("Invt_Store")
      .get(uri1 + "inventory/store/${StoreID}?productId=${ProductID}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
  }

  object SearchSuggestion {
    val AutoSuggest = feed(search_feeder).exec(http("Search_AutoSuggest")
      .get(uri1 + "search/autosuggest/${SearchTerm}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val SiteContent = feed(search_feeder).exec(http("Search_SiteContent")
      .get(uri1 + "search/sitecontent").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val SiteSearch = feed(search_feeder).exec(http("Search_SiteSearch")
      .get(uri1 + "search/sitesearch/${SearchTerm}?orderBy=&pageSize=48&pageNumber=1&facet=").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val FacetSearch = feed(search_feeder).exec(http("Search_FacetSearch")
      .get(uri1 + "search?facets=${Facet}&orderBy=1&pageSize=5&categoryId=${Category}&pageNumber=1").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
  }

  val scn1 = scenario("Product").
    exec(
      Product.Carousel,
      Product.APIByProduct,
      Product.FacetCategory
    )

  val scn2 = scenario("Category").
    exec(
      Category.MultiCategory
    )

  val scn3 = scenario("Seo").
    exec(
      Seo.ProductName,
      Seo.CategoryName
    )

  val scn4 = scenario("Inventory").
    exec(
      Inventory.OnlineAndStore,
      Inventory.Online,
      Inventory.Store
    )

  val scn5 = scenario("SearchSuggestion").
    exec(
      SearchSuggestion.AutoSuggest,
      SearchSuggestion.SiteContent,
      SearchSuggestion.SiteSearch,
      SearchSuggestion.FacetSearch
    )

  setUp(scn1.inject(atOnceUsers(1)),
    scn2.inject(atOnceUsers(1)),
    scn3.inject(atOnceUsers(1)),
    scn4.inject(atOnceUsers(1)),
    scn5.inject(atOnceUsers(1))
  ).protocols(httpProtocol)
}